scripts = ('proj', 'colortest', 'rmscratch', 'gitdone')
path_scripts = ('proj', 'colortest', 'rmscratch')
subdir = 'unix'

p.install({x: f'~/.scripts/{subdir}/{x}' for x in scripts})
# p.install({'gitconfig': '~/.gitconfig'})
p.link(
    {f'~/.scripts/{subdir}/{s}': f'~/.scripts/path/{s}'
     for s in path_scripts})
