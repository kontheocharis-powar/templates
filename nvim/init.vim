" Ensure we are not in vscode:
if exists('g:vscode')
    set noshowmode                     " don't show --INSERT-- etc. in echo (can be deduced from cursor)
    set ignorecase
    set smartcase                      " smart case when searching
    set updatetime=100
    set ttimeoutlen=0
    set autochdir
    let mapleader = "\<Space>"

    " Window stuff
    nmap <C-a> <C-w>
    nmap <C-w><C-h> <C-w>h
    nmap <C-w><C-j> <C-w>j
    nmap <C-w><C-k> <C-w>k
    nmap <C-w><C-i> nop
    nmap <C-w><C-l> <C-w>l

    nmap <C-w><C-q> <C-w>q
    nmap <C-w>q <cmd>call VSCodeNotify('workbench.action.closeEditorsAndGroup')<cr>

    " Switch tabs
    nmap <silent> <C-a>u gT
    nmap <silent> <C-a>p gt

    " Insert lines above/below in normal mode
    nmap <silent> zj o<Esc><Up>
    nmap <silent> zk O<Esc><Down>

    " Clipboard
    vmap <leader>p "+p
    vmap <leader>y "+y
    nmap <leader>p "+p
    nmap <leader>y "+yy

    " Indent when paste
    " nmap <c-p> p=`]

    " Movement
    nmap <silent> k gk
    nmap <silent> j gj

    " Stop all highlighting
    nmap <silent> <esc><esc> :noh<CR>

    " Open vimrc
    nmap <silent> <leader>rc :e ~/.pt/nvim/init.vim<CR>

    " Other buffer
    nmap <silent> <C-a>b :b#<CR>

    " Code actions
    nmap <silent> <leader>aa <cmd>call VSCodeNotify('editor.action.formatDocument')<cr>
    nmap <silent> <leader>fa <cmd>call VSCodeNotify('editor.action.quickFix')<cr>
    nmap <silent> <leader>ff <cmd>call VSCodeNotify('workbench.action.quickOpen')<cr>
    nmap <silent> <leader>fp <cmd>call VSCodeNotify('workbench.action.showCommands')<cr>
    nmap <silent> <leader>fs <cmd>call VSCodeNotify('workbench.action.showAllSymbols')<cr>
    nmap <silent> <leader>s <cmd>call VSCodeNotify('workbench.action.files.save')<cr>
    nmap <silent> <leader>cc <cmd>call VSCodeNotify('workbench.action.closePanel')<cr>
    nmap <silent> <leader>rn <cmd>call VSCodeNotify('editor.action.rename')<cr>
    nmap <silent> <leader>gn <cmd>call VSCodeNotify('editor.action.marker.nextInFiles')<cr>
    nmap <silent> <leader>gp <cmd>call VSCodeNotify('editor.action.marker.prevInFiles')<cr>

    " Folding
    nmap <silent> zc <cmd>call VSCodeNotify('editor.fold')<cr>
    nmap <silent> zo <cmd>call VSCodeNotify('editor.unfold')<cr>
    nmap <silent> za <cmd>call VSCodeNotify('editor.toggleFold')<cr>
    nmap <silent> zr <cmd>call VSCodeNotify('editor.unfoldAllMarkerRegions')<cr>
    nmap <silent> zR <cmd>call VSCodeNotify('editor.unfoldRecursively')<cr>
    nmap <silent> zm <cmd>call VSCodeNotify('editor.foldAllMarkerRegions')<cr>
    nmap <silent> zM <cmd>call VSCodeNotify('editor.foldRecursively')<cr>

    " Comments
    xmap <silent> gc  <Plug>VSCodeCommentary
    nmap <silent> gc  <Plug>VSCodeCommentary
    omap <silent> gc  <Plug>VSCodeCommentary
    nmap <silent> gcc <Plug>VSCodeCommentaryLine

    set rtp+={{ nvim.fzf_path }}

    call plug#begin(stdpath('data') . '/plugged')

    " Better search-and-replace
    Plug 'tpope/vim-abolish'

    " Arguments as text objects
    Plug 'AndrewRadev/sideways.vim'

    " Surround things with brackets "
    Plug 'tpope/vim-surround'

    " Alignment based on a character
    Plug 'junegunn/vim-easy-align'

    call plug#end()

    if has_key(plugs, 'sideways.vim')
        omap <silent> aa <Plug>SidewaysArgumentTextobjA
        xmap <silent> aa <Plug>SidewaysArgumentTextobjA
        omap <silent> ia <Plug>SidewaysArgumentTextobjI
        xmap <silent> ia <Plug>SidewaysArgumentTextobjI
    endif

    if has_key(plugs, 'vim-easy-align')
        " Use ga for aligning
        xmap <silent> ga <Plug>(EasyAlign)
        nmap <silent> ga <Plug>(EasyAlign)
    endif
else
" General:
    set title titlestring=             " fix title
    set shiftwidth=4                   " how many columns to indent
    set tabstop=4                      " how many spaces per tab
    set expandtab                      " turn tab into spaces
    set autoindent                     " auto-indenting on enter etc.
    set cindent                        " c-style indenting (curly brakcets)
    set cino=g0,N-s,j1,:0,(s,m2
    set noshowmode                     " don't show --INSERT-- etc. in echo (can be deduced from cursor)
    set linebreak                      " wrap lines while honouring word breaks
    set listchars=extends:▶,precedes:◀ " set characters for line continuation off screen
    set hidden
    set mouse=a                        " mouse support
    set fillchars+=vert:┃              " remove vertical separator
    set fillchars+=eob:\               " remove tilde
    set shortmess+=c
    set showbreak=\ \                  " indicate wrapped line
    set splitbelow                     " horizontal split above instead of below
    set splitright                     " vertical split right instead of left
    set nocompatible                   " disable legacy vi mode
    set breakindent
    set foldlevel=99
    set autochdir
    set conceallevel=0                 " conceal unless special character set
    set undofile                       " persistent undo
    set undodir=~/.local/share/nvim/undo
    if has('nvim-0.4') || has('nvim-0.5')
        set formatoptions+=np
        set signcolumn=yes:1
    else
        set formatoptions+=n
        set signcolumn=yes
    endif
    set ignorecase
    set smartcase                      " smart case when searching
    set updatetime=100
    set ttimeoutlen=0
    set matchpairs+=<:>                " angular bracket matching

    if !exists('g:minimal_mode')
        let g:minimal_mode=0
    endif

    let g:node_host_prog = '{{ nvim.node_host }}'
    " let g:python_host_prog = '/usr/bin/python2'
    let g:python3_host_prog = '{{ nvim.python3_host }}'

" DeinVim:
    " set runtimepath+=/Users/constantine/.cache/dein/repos/github.com/Shougo/dein.vim

    " for fzf
    set rtp+={{ nvim.fzf_path }}

    call plug#begin(stdpath('data') . '/plugged')

    " Plug 'dstein64/vim-startuptime'

    " Support for indent text objects
    Plug 'michaeljsmith/vim-indent-object'

    " Arguments as text objects
    " Plug 'b4winckler/vim-angry'
    Plug 'wellle/targets.vim'

    " Show preview of substitutions
    Plug 'markonm/traces.vim'

    " Solarized8 color scheme (improved solarized)
    Plug 'lifepillar/vim-solarized8'

    " Prisma and js
    Plug 'pantharshit00/vim-prisma'
    " Plug 'pangloss/vim-javascript'

    " Multicursor
    " Plug 'terryma/vim-multiple-cursors'

    if g:minimal_mode == 0
        " Visual block cmd"
        " Plug 'vim-scripts/vis'

        " Discord
        " Plug 'aurieh/discord.nvim', {'do': ':UpdateRemotePlugins'}

        "GnuPG"
        Plug 'jamessan/vim-gnupg'

        " Surround things with brackets "
        Plug 'tpope/vim-surround'

        " Ripgrep
        Plug 'jremmen/vim-ripgrep'

        " Search and replace
        Plug 'brooth/far.vim'

        " Better search-and-replace
        Plug 'tpope/vim-abolish'

        " Code formatting
        " Plug 'google/vim-maktaba'
        " Plug 'google/vim-codefmt'
        " Plug 'google/vim-glaive'

        " Better code formatting
        Plug 'sbdchd/neoformat'

        " Git gutter
        Plug 'airblade/vim-gitgutter'

        " Support for selecting lines as text objects
        Plug 'kana/vim-textobj-user'
        Plug 'kana/vim-textobj-line'

        " Fix obscene default swapfile behaviour
        Plug 'gioele/vim-autoswap'

        " Insert/delete div or quotes in pairs
        " Plug 'jiangmiao/auto-pairs'
        " Plug 'rstacruz/vim-closer'
        Plug 'cohama/lexima.vim'

        " Commenting out stuff
        Plug 'tpope/vim-commentary'

        " Better :make, among other things
        Plug 'tpope/vim-dispatch'

        " Autodetect indent
        Plug 'tpope/vim-sleuth'
        " Plug 'zsugabubus/crazy8.nvim'

        " Highlight matching braces

        " Auto Ctags
        " Plug 'ludovicchabant/vim-gutentags'

        " Kill buffer without killing window
        Plug 'vim-scripts/bufkill.vim'

        " Unix-like commands
        Plug 'tpope/vim-eunuch'

        " Better substitute for Netrw
        Plug 'scrooloose/nerdtree'

        " Icons for NERDTree
        Plug 'ryanoasis/vim-devicons'

        " Alignment based on a character
        Plug 'junegunn/vim-easy-align'

        " Automatic bullet insertion
        Plug 'dkarter/bullets.vim'

        " Light and customizable statusline
        Plug 'itchyny/lightline.vim'

        " JS better syntax
        " Plug 'othree/yajs.vim'

        " Pandoc IDE
        " Plug 'vim-pandoc/vim-pandoc-syntax'
        " Plug 'vim-pandoc/vim-pandoc'

        " Treesitter
        if has('nvim-0.5')
            Plug 'https://github.com/nvim-treesitter/nvim-treesitter'
        endif

        " Git
        Plug 'tpope/vim-fugitive'
        Plug 'christoomey/vim-conflicted'

        " Project settings
        Plug 'krisajenkins/vim-projectlocal'

        " Get root directory
        Plug 'airblade/vim-rooter'

        " Switch between .c and .h"
        Plug 'derekwyatt/vim-fswitch'

        " Jinja2 syntax
        Plug 'Glench/Vim-Jinja2-Syntax'

        " Hash
        Plug 'hash-org/hash.vim'

        " Fuzzy file search
        Plug 'junegunn/fzf', {'do': ':call fzf#install()'}
        Plug 'junegunn/fzf.vim'

        " Latex IDE
        Plug 'lervag/vimtex'

        " Rust plugin
        " Plug 'rust-lang/rust.vim'
        Plug 'pest-parser/pest.vim'

        " Conceal for LaTeX
        " Plug 'KeitaNakamura/tex-conceal.vim'

        " Tags
        " Plug 'alvan/vim-closetag'
        " Plug 'AndrewRadev/tagalong.vim'

        " Typescript & js
        " Plug 'maxmellon/vim-jsx-pretty'
        " Plug 'leafgarland/typescript-vim'

        " Unicode snippets
        Plug 'danielwe/vim-unicode-snippets'

        if has('python3') || has('python')
            " Snippet engine
            " Plug 'SirVer/ultisnips'

            " Snippets for UltiSnips
            Plug 'honza/vim-snippets'

            if has('node')
                " IDE
                Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
                Plug 'josa42/vim-lightline-coc'
                Plug 'mlaursen/vim-react-snippets'

            endif
        endif

        " Fix slow folds
        Plug 'Konfekt/FastFold'

    endif

    call plug#end()

" Colorscheme:
    filetype plugin indent on
    syntax enable

    set background=dark

    " Send resize signal on open
    autocmd VimEnter * :silent exec "!kill -s SIGWINCH $PPID"

    if has_key(plugs, 'vim-solarized8')
        " Some solarized8 settings
        let g:solarized_use16 = 1
        let g:solarized_term_italics = 1
        let g:solarized_extra_hi_groups = 1
        colorscheme solarized8_flat
    endif

    " @@Temporary: chdir hack: https://github.com/gelguy/wilder.nvim/issues/62
    autocmd BufEnter * call Chdir()
    function! Chdir()
        let l:dir = expand('%:p:h')
        if isdirectory(l:dir)
            execute 'cd ' . l:dir
        endif
    endfunction

" Filetype:
    " Indent folding
    " autocmd FileType vim setlocal foldmethod=indent

    " " Syntax folding
    " autocmd FileType typescript setlocal foldmethod=syntax
    " autocmd FileType typescript.tsx setlocal foldmethod=syntax
    " autocmd FileType c,cpp setlocal foldmethod=syntax
    " autocmd FileType yaml setlocal foldmethod=indent

    " Syntax toplevel
    autocmd FileType tex syntax spell toplevel

    " Make recursive
    " autocmd FileType tex,bib set makeprg=mk

    " Python linting
    " autocmd FileType python let &makeprg='cd "$(~/.scripts/get-root)" && mypy --strict *.py'
    " autocmd FileType python nnoremap <leader>ac :execute 'Dispatch cd "$(~/.scripts/get-root)" && mypy --strict *.py'<CR>
    " autocmd FileType python set errorformat=%f:%l:\ %m

    " Formatting with <leader>aa
    " autocmd FileType rust nnoremap <leader>aa :RustFmt<CR>
    nnoremap <leader>aa :FormatCode<CR>


    " Rust
    " let g:rust_fold = 1
    " let g:cargo_makeprg_params = "check"
    autocmd FileType rust nnoremap <leader>ac :execute 'Dispatch ' . &makeprg . ' check'<CR>

    " Python
    let g:python_highlight_space_errors = 0

" Spellchecking:
    " Set filetypes to spellcheck
    autocmd FileType latex,tex,md,markdown,mail,gitcommit setlocal spell

    " Disable checking sentence capitalisation
    autocmd FileType latex,tex,md,markdown,mail setlocal spellcapcheck=

" Keybindings:
    if g:minimal_mode == 1
        " Quickly quit
        nnoremap q :q<CR>
    endif

    " Set leader to something better
    let mapleader = "\<Space>"
    let maplocalleader = ','

    " General commands

    " Disable stupid man pages
    nnoremap K <nop>
    vnoremap K <nop>


    " Clipboard
    noremap <leader>p "+p
    noremap <leader>y "+y

    nnoremap <C-w> ggVG
    nnoremap <C-a> <C-w>

    " Open vimrc
    nnoremap <silent> <leader>rc :e $MYVIMRC<CR>
    nnoremap <silent> <leader>so :source $MYVIMRC<CR>

    " Highlight group under cursor
    nnoremap <silent> <leader>gh :echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')<CR>

    " Silent open
    nnoremap <leader>oo :silent !open

    " Stop all highlighting
    nnoremap <silent> <esc><esc> :noh<CR>

    " Switch tabs
    noremap <silent> <C-a>u gT
    noremap <silent> <C-a>p gt

    " Toggle greek
    noremap <silent> <leader>gg :call ToggleGreek()<CR>

    function! ToggleGreek()
        if &iminsert
            set iminsert=0
        else
            set iminsert=1
        endif
    endfunction

    " Navigate by visual lines
    noremap <silent> k gk
    noremap <silent> j gj

    " Move lines
    " nnoremap <silent> <C-j> J
    " nnoremap <silent> J :move +1<CR>==
    " nnoremap <silent> K :move -2<CR>==

    " Indent when paste
    nnoremap <c-p> p=`]

    " Insert lines above/below in normal mode
    nnoremap <silent> zj o<Esc><Up>
    nnoremap <silent> zk O<Esc><Down>

    " Remove smart quotes from selection
    xnoremap <leader>rq :!sed $'s/[“”]/"/g; s/[‘’]/\'/g'<CR>

    " Break lines
    nnoremap <leader>bl :%s/\.\n/\.\r\r/g<CR>:%s/\.\ /\.\r/g<CR>

    inoremap <C-y> <C-k>
    " Digraphs

    " Tabs
    nnoremap <silent> <C-a>1 1gt
    nnoremap <silent> <C-a>2 2gt
    nnoremap <silent> <C-a>3 3gt
    nnoremap <silent> <C-a>4 4gt
    nnoremap <silent> <C-a>5 5gt
    nnoremap <silent> <C-a>6 6gt
    nnoremap <silent> <C-a>7 7gt
    nnoremap <silent> <C-a>8 8gt
    nnoremap <silent> <C-a>9 9gt
    nnoremap <silent> <C-a>0 10gt

    " Other buffer
    nnoremap <silent> <C-a>b :b#<CR>

    " Make command mode more reasonable
    cnoremap <expr><S-Tab>  pumvisible() ? "\<Left>" : ""
    cnoremap <expr><C-j>  pumvisible() ? "\<Right>" : "\<Down>"
    cnoremap <expr><C-k>  pumvisible() ? "\<Left>" : "\<Up>"
    cnoremap <expr><down>  pumvisible() ? "\<Right>" : "\<Down>"
    cnoremap <expr><up>  pumvisible() ? "\<Left>" : "\<Up>"

    " Change the word under cursor
    nnoremap <leader>wr :%s/\<<C-r><C-w>\>//g<Left><Left>

    " Star command without moving cursor
    nnoremap * *N

" Languages:
    " Add greek keymap
    set keymap=greek

    " Off by default
    set iminsert=0

    " Search with current keymap
    set imsearch=-1

    " Spelling in english and greek
    set spelllang=en,el

" Plugins:
    if has_key(plugs, 'vim-dispatch')
        " Make project
        nmap <silent> <leader>cm :Make<CR>

        " Make BG project
        nmap <silent> <leader>cb :Make!<CR>

        " Next quickfix
        nmap <silent> cn :cnext<CR>

        " Prev quickfix
        nmap <silent> cp :cprevious<CR>

        " Open quickfix
        nmap <silent> <leader>co :Copen<CR>

        " Close quickfix
        nmap <silent> <leader>cc :ccl<CR>
    endif

    if has_key(plugs, 'bufkill.vim')
        " Kill buffer
        noremap <silent> <C-a>d :BD<CR>
    endif

    if has_key(plugs, 'vim-yapf-format')
        let g:yapf_format_yapf_location = '/usr/local/bin/yapf'
        " autocmd FileType python nnoremap <buffer> <leader>aa :YapfFullFormat<CR>
    endif

    if has_key(plugs, 'vim-glaive')
        call glaive#Install()
    endif

    if has_key(plugs, 'vim-codefmt')
        Glaive codefmt prettier_executable="/usr/local/bin/prettier"
        autocmd FileType typescript,typescriptreact,javascript,typescript.tsx,javascript,javascript.jsx,javascriptreact let b:codefmt_formatter = 'prettier'
        noremap <leader>aa :FormatCode<CR>
    endif

    if has_key(plugs, 'neoformat')
        let g:neoformat_enabled_haskell = ['ormolu']
        let g:neoformat_enabled_javascript = ['prettier']
        let g:neoformat_java_google = {
            \ 'exe': 'google-java-format',
            \ 'args': ['-'],
            \ 'stdin': 1,
            \ }

        let g:neoformat_enabled_java = ['google']
        let g:neoformat_try_formatprg = 1
        noremap <leader>aa :Neoformat<CR>
    endif

    if has_key(plugs, 'lightline.vim')
        " Configure lightline
        let g:lightline = {}
        let g:lightline.colorscheme = 'solarized8'

        let lightline_left = [ 'readonly', 'filename', 'modified' ]
        if has_key(plugs, 'coc.nvim')
            let g:lightline.component_expand = {
                        \   'linter_warnings': 'lightline#coc#warnings',
                        \   'linter_errors': 'lightline#coc#errors',
                        \   'linter_info': 'lightline#coc#info',
                        \   'linter_hints': 'lightline#coc#hints',
                        \   'linter_ok': 'lightline#coc#ok',
                        \   'status': 'lightline#coc#status',
                        \ }

            " Set color to the components:
            let g:lightline.component_type = {
                        \   'linter_warnings': 'warning',
                        \   'linter_errors': 'error',
                        \   'linter_info': 'left',
                        \   'linter_hints': 'left',
                        \   'linter_ok': 'left',
                        \ }
            let g:lightline#coc#indicator_warnings = "⭘ "
            let g:lightline#coc#indicator_errors = "⭘ "
            let g:lightline#coc#indicator_info = "⭘ "
            let g:lightline#coc#indicator_hints = "⭘ "
            let lightline_left = [ 'readonly', 'filename', 'coc_errors', 'coc_warnings', 'modified' ]
            call lightline#coc#register()
        endif
        let g:lightline.active = {
            \'left': [ [], [], lightline_left ],
            \'right': [ [], [], ['keymap', 'tab-rs', 'percent', 'lineinfo', 'filetype' ] ],
        \}

        let g:lightline.inactive = {
            \'left': [ [ 'filename' ] ],
            \'right': [ [], [ 'lineinfo', 'percent' ] ],
        \}

        let g:lightline.tabline = {
            \'left': [ [ 'tabs' ] ],
            \'right': [ [] ],
        \}

        let g:lightline.component_function = {
            \'keymap': 'GetKeymap',
            \'tab-rs': 'GetTabRs',
        \}

        function! GetKeymap()
            return &iminsert == 0 ? '' : 'Ω'
        endfunction

        function! GetTabRs()
            return $TAB
        endfunction

        let g:lightline.subseparator = { 'left': ' ', 'right': ' ' }
        let g:lightline.tabline_separator = { 'left': ' ', 'right': ' ' }
    endif

    if has_key(plugs, 'lexima.vim')
        call lexima#add_rule({'char': '$', 'input_after': '$', 'filetype': 'latex'})
        call lexima#add_rule({'char': '$', 'at': '\%#\$', 'leave': 1, 'filetype': 'latex'})
        call lexima#add_rule({'char': '<BS>', 'at': '\$\%#\$', 'delete': 1, 'filetype': 'latex'})
    endif

    if has_key(plugs, 'nerdtree')
        nmap <silent> <leader>ft :NERDTree<CR>

        " Remove useless elements
        let NERDTreeMinimalUI = 1

        " Better arrows
        let g:NERDTreeDirArrowExpandable = ""
        let g:NERDTreeDirArrowCollapsible = ""

        let g:WebDevIconsUnicodeDecorateFolderNodes = v:true
        let g:WebDevIconsNerdTreeBeforeGlyphPadding = ""

    endif

    if has_key(plugs, 'limelight.vim')
        "Limelight
        nmap <silent> <leader>vv :Limelight!!<CR>
        xmap <silent> <leader>vv :Limelight!!<CR>

        let g:limelight_conceal_ctermfg = 10
    endif

    if has_key(plugs, 'vim-javascript')
        let g:javascript_plugin_jsdoc = 1
    endif

    if has_key(plugs, 'vim-rooter')
        let g:rooter_manual_only = 1
        let g:rooter_patterns = ['.proj-root', '.git/']
    endif

    if has_key(plugs, 'vim-gutentags')
        let g:gutentags_project_root = ['.proj-root', '.git/']
        let g:gutentags_ctags_tagfile = '.tags'

        let g:gutentags_generate_on_missing = 0
        let g:gutentags_generate_on_new = 0
        let g:gutentags_generate_on_empty_buffer = 0

        let g:gutentags_cache_dir = "~/.cache/gutentags"
    endif

    if has_key(plugs, 'vimtex')
        " Enable folds
        let g:vimtex_fold_enabled = 1

        " Ensure latex
        let g:tex_flavor = 'latex'

        autocmd FileType latex set commentstring=%%s

        " Surround environments
        let b:surround_{char2nr('e')} = "\\begin{\1environment: \1}\n\t\r\n\\end{\1\1}"
        let b:surround_{char2nr('c')} = "\\\1command: \1{\r}"
    endif

    if has_key(plugs, 'tex-conceal.vim')
        " Conceal all except superscripts
        " let g:tex_conceal="abdgm"
        let g:tex_conceal=""
    endif

    if has_key(plugs, 'vim-easy-align')
        " Use ga for aligning
        xmap ga <Plug>(EasyAlign)
        nmap ga <Plug>(EasyAlign)

        " Align all by equals (=)"
        nmap <leader>ga= ggVGga=
    endif

    if has_key(plugs, 'far.vim')
        let g:far#source = 'rgnvim'
        let g:far#window_layout = 'bottom'
        let g:far#preview_window_layout = 'right'
        let g:far#auto_preview_on_start = 0

        " Search and replace for word under cursor
        nnoremap <leader>wf :Far <C-r><C-w>  / -w<left><left><left><left><left>
    endif

    if has_key(plugs, 'vim-ripgrep')
        let g:rg_derive_root = 1
        " Search for word under cursor
        nnoremap <leader>ww :Rg '\b<C-r><C-w>\b'<CR>
    endif

    if has_key(plugs, 'bullets.vim')
        " Filetypes for automatic insertion of bullets
        let g:bullets_enabled_file_types = [
            \'markdown',
            \'text',
            \'tex',
            \'mail',
            \'org',
            \'gitcommit',
            \'scratch'
        \]
        let g:bullets_pad_right = 1
        let g:bullets_outline_levels = ['num', 'std-']
    endif

    if has_key(plugs, 'vim-closetag')
        let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.tsx'
        let g:closetag_xhtml_filenames = '*.xhtml,*.jsx,*.tsx'
        let g:closetag_xhtml_filetypes = 'xhtml,javascript.jsx,jsx,typescript.tsx,tsx'
        let g:closetag_emptyTags_caseSensitive = 1
        let g:closetag_regions =  {
            \'typescript.tsx': 'jsxRegion,tsxRegion',
            \'javascript.jsx': 'jsxRegion',
        \}
    endif

    if has_key(plugs, 'AndrewRadev/tagalong.vim')
        let g:tagalong_additional_filetypes = ['javascript.jsx', 'typescript.tsx']
    endif

    if has_key(plugs, 'vim-pandoc-syntax')
        let g:pandoc#syntax#codeblocks#embeds#langs = ["python", "c"]
        let g:pandoc#syntax#conceal#use = 0
        hi! pandocNewline ctermbg=0

        if has_key(plugs, 'vim-pandoc')
            let g:pandoc#folding#fdc = 0
            let g:pandoc#modules#disabled = ["completion"]
        else
            augroup pandoc_syntax
                au! BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc
            augroup END
        endif
    endif

    if has_key(plugs, 'rainbow')
        " Rainbow brackets (easier to see matches)
        let g:rainbow_active = 1

        " Colors
        let g:rainbow_conf = {
            \'ctermfgs': ['10'],
            \'operators': '_,_',
            \'separately': {
            \   'nerdtree': 0,
            \}
        \}
    endif

    if has_key(plugs, 'vim-gofmt')
        " autocmd FileType go nnoremap <leader>aa :GoFmt<CR>
    endif

    if has_key(plugs, 'auto-pairs')
        let g:AutoPairsMultilineClose = 0
    endif

    if has_key(plugs, 'targets.vim')
        autocmd User targets#mappings#user call targets#mappings#extend({
                    \ 'a': {'argument': [{'o': '[({<[]', 'c': '[]>})]', 's': ','}]}
                    \ })
    endif

    if has_key(plugs, 'traces.vim')
        let g:traces_abolish_integration = 1
    endif

    if has_key(plugs, 'coc.nvim')
        " General mappings
        inoremap <silent><expr> <TAB>
                    \ pumvisible() ? coc#_select_confirm() :
                    \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
                    \ <SID>check_back_space() ? "\<TAB>" :
                    \ coc#refresh()

        function! s:check_back_space() abort
            let col = col('.') - 1
            return !col || getline('.')[col - 1]  =~# '\s'
        endfunction

        let g:coc_snippet_next = '<tab>'
        let g:coc_snippet_prev = '<S-tab>'

        " Auto signature help
        autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
        inoremap <silent><expr> <C-space> coc#refresh()
        inoremap <silent> <C-f> <C-\><C-O>:call CocActionAsync('showSignatureHelp')<CR>

        " Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
        nmap <silent> gp <Plug>(coc-diagnostic-prev)
        nmap <silent> gn <Plug>(coc-diagnostic-next)

        " GoTo code navigation.
        nmap <silent> gd <Plug>(coc-definition)
        nmap <silent> gy <Plug>(coc-type-definition)
        nmap <silent> gi <Plug>(coc-implementation)
        nmap <silent> gr <Plug>(coc-references)

        " Use K to show documentation in preview window.
        nnoremap <silent> K :call <SID>show_documentation()<CR>

        function! s:show_documentation()
          if (index(['vim','help'], &filetype) >= 0)
            execute 'h '.expand('<cword>')
          else
            call CocActionAsync('doHover')
          endif
        endfunction

        " Highlight the symbol and its references when holding the cursor.
        " autocmd CursorHold * silent call CocActionAsync('highlight')

        " Symbol renaming.
        nmap <leader>rn <Plug>(coc-rename)

        " Map function and class text objects
        " NOTE: Requires 'textDocument.documentSymbol' support from the language server.
        xmap if <Plug>(coc-funcobj-i)
        omap if <Plug>(coc-funcobj-i)
        xmap af <Plug>(coc-funcobj-a)
        omap af <Plug>(coc-funcobj-a)
        xmap ic <Plug>(coc-classobj-i)
        omap ic <Plug>(coc-classobj-i)
        xmap ac <Plug>(coc-classobj-a)
        omap ac <Plug>(coc-classobj-a)

        " Add `:OR` command for organize imports of the current buffer.
        command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')
        nnoremap <silent><nowait> <leader>or  :OR<CR>

        " Mappings for CoCList
        " Show all diagnostics.
        nnoremap <silent><nowait> <leader>ea  :<C-u>CocList diagnostics<cr>
        " Next, prev diagnostic
        nnoremap <silent><nowait> <leader>dn  :<C-u>CocNext diagnostics<cr>
        nnoremap <silent><nowait> <leader>dp  :<C-u>CocPrev diagnostics<cr>
        " Manage extensions.
        nnoremap <silent><nowait> <leader>ee  :<C-u>CocList extensions<cr>
        " Show commands.
        nnoremap <silent><nowait> <leader>ec  :<C-u>CocList commands<cr>
        " Find symbol of current document.
        nnoremap <silent><nowait> <leader>eo  :<C-u>CocList outline<cr>
        " Search workspace symbols.
        nnoremap <silent><nowait> <leader>es  :<C-u>CocList -I symbols<cr>
        " Do default action for next item.
        nnoremap <silent><nowait> <leader>ej  :<C-u>CocNext<CR>
        " Do default action for previous item.
        nnoremap <silent><nowait> <leader>ek  :<C-u>CocPrev<CR>
        " Resume latest coc list.
        nnoremap <silent><nowait> <leader>ep  :<C-u>CocListResume<CR>

        nnoremap <silent><nowait> <leader>fx  :CocFix<CR>
        nmap <silent> <leader>fa  v<Plug>(coc-codeaction-selected)
        vmap <silent> <leader>fa  <Plug>(coc-codeaction-selected)

        " Highlight hints
        hi! CocHintSign cterm=NONE ctermfg=10 ctermbg=bg
    endif

    if has_key(plugs, 'ultisnips') && has_key(plugs, 'coc.nvim')
        " Tab snippets
        " inoremap <silent><expr> <TAB>
        "   \ pumvisible() ? coc#_select_confirm() :
        "   \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
        "   \ <SID>check_back_space() ? "\<TAB>" :
        "   \ coc#refresh()

        " function! s:check_back_space() abort
        "   let col = col('.') - 1
        "   return !col || getline('.')[col - 1]  =~# '\s'
        " endfunction

        " let g:coc_snippet_next = '<tab>'
        " let g:coc_snippet_prev = '<s-tab>'

        " Keybindings
        let g:UltiSnipsExpandTrigger="<nop>"
        let g:UltiSnipsJumpForwardTrigger="<tab>"
        let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
    endif


    if has_key(plugs, 'vim-fswitch')
        nmap <silent> <leader>of :FSHere<cr>
        nmap <silent> <leader>ol :FSSplitRight<cr>
        nmap <silent> <leader>oj :FSSplitBelow<cr>
    endif

    if has_key(plugs, 'vim-gitgutter')
        let g:gitgutter_enabled = 0
        nmap <silent> <leader>gu :GitGutterToggle<CR>
    endif

    if has_key(plugs, 'nvim-treesitter')
lua <<EOF
require'nvim-treesitter.configs'.setup {
    highlight = {
      enable = true,                    -- false will disable the whole extension
    },
    indent = {
      enable = true,                    -- false will disable the whole extension
    },
    incremental_selection = {
      enable = true,
      keymaps = {                       -- mappings for incremental selection (visual mappings)
        init_selection = "gnn",         -- maps in normal mode to init the node/scope selection
        node_incremental = "grn",       -- increment to the upper named parent
        scope_incremental = "grc",      -- increment to the upper scope (as defined in locals.scm)
        node_decremental = "grm",       -- decrement to the previous node
      }
    },
    refactor = {
      highlight_definitions = {
        enable = false
      },
      highlight_current_scope = {
        enable = false
      },
      smart_rename = {
        enable = true,
        keymaps = {
          smart_rename = "grr"          -- mapping to rename reference under cursor
        }
      },
      navigation = {
        enable = true,
        keymaps = {
          goto_definition = "gnd",      -- mapping to go to definition of symbol under cursor
          list_definitions = "gnD"      -- mapping to list all definitions in current file
        }
      }
    },
    textobjects = { -- syntax-aware textobjects
    enable = false,
    disable = { "ocaml", "haskell" },
    keymaps = {
        ["iL"] = { -- you can define your own textobjects directly here
          python = "(function_definition) @function",
          cpp = "(function_definition) @function",
          c = "(function_definition) @function",
          java = "(method_declaration) @function"
        },
        -- or you use the queries from supported languages with textobjects.scm
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
        ["aC"] = "@class.outer",
        ["iC"] = "@class.inner",
        ["ac"] = "@conditional.outer",
        ["ic"] = "@conditional.inner",
        ["ae"] = "@block.outer",
        ["ie"] = "@block.inner",
        ["al"] = "@loop.outer",
        ["il"] = "@loop.inner",
        ["is"] = "@statement.inner",
        ["as"] = "@statement.outer",
        ["ad"] = "@comment.outer",
        ["am"] = "@call.outer",
        ["im"] = "@call.inner"
      }
    },
    ensure_installed = {
    } -- one of "all", "language", or a list of languages
}
EOF
        set foldmethod=expr
        set foldexpr=nvim_treesitter#foldexpr()
    endif

    if has_key(plugs, 'fzf.vim')
        let g:fzf_command_prefix = 'Fzf'

        " Fzf in directory of current buffer
        nmap <silent> <leader>fs :FzfFiles<CR>

        " Fzf in root
        nmap <silent> <leader>ff :execute 'FzfFiles' FindRootDirectory()<CR>

        " Home fzf
        nmap <silent> <leader>fh :FzfFiles ~/<CR>

        " Root fzf
        nmap <silent> <leader>fr :FzfFiles /<CR>

        " Fzf for buffers
        nmap <silent> <leader>fb :FzfBuffers<CR>

        " Fzf for tags
        nmap <silent> <leader>tt :FzfTags<CR>

        " Setup colors for Fzf
        let g:fzf_colors = {
            \'fg':      ['fg', 'Normal'],
            \'bg':      ['bg', 'Normal'],
            \'hl':      ['fg', 'Keyword'],
            \'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
            \'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
            \'hl+':     ['fg', 'Normal'],
            \'info':    ['fg', 'Normal'],
            \'border':  ['fg', 'Ignore'],
            \'prompt':  ['fg', 'Conditional'],
            \'pointer': ['bg', 'StatusLine'],
            \'marker':  ['fg', 'Keyword'],
            \'spinner': ['fg', 'Label'],
            \'header':  ['fg', 'Comment']
        \}

        " Fill 40% of screen
        let g:fzf_layout = { 'down': '~40%' }

        " No preview
        let g:fzf_preview_window = ''

        " Remove status during fzf
        autocmd! FileType fzf
        autocmd  FileType fzf set laststatus=0 | autocmd WinLeave <buffer> set laststatus=2
    endif

" Colorscheme Fixes:
    if has_key(plugs, 'vim-solarized8')
        hi! Delimiter ctermfg=10

        hi! ErrorMsg cterm=NONE ctermfg=1 ctermbg=bg

        hi! SpecialComment ctermfg=10
        hi! Folded cterm=bold ctermfg=10 ctermbg=0
        " Fix cursorline (underline by default)
        hi! Normal ctermbg=NONE
        hi! VertSplit ctermfg=0 ctermbg=NONE
        hi! CursorLine cterm=NONE ctermbg=0

        " Floating fix"
        hi! Pmenu ctermfg=12 ctermbg=0 cterm=NONE
        hi! PmenuSbar ctermfg=NONE ctermbg=0 cterm=NONE
        hi! PmenuSel ctermfg=15 ctermbg=10 cterm=NONE
    endif

" Syntax Fixes:
    " Rust modifications
    " syn match rustType "\v[A-Z][a-z0-9][a-zA-Z0-9]+"
    " syn match rustModPath "\v[A-Z][a-z0-9][a-zA-Z0-9]+(::\v[A-Z][a-z0-9][a-zA-Z0-9]+)@="
    " syn match rustEnumVariant "\v([A-Z][a-z0-9][a-zA-Z0-9]+::)@<=[A-Z][a-z0-9][a-zA-Z0-9]+"

    " autocmd Syntax * syn match customCommentTag "\v\@<\w+>" containedin=.*Comment,vimCommentTitle,cCommentL
    " hi! customCommentTag cterm=BOLD

" Folding:
    if g:minimal_mode == 0
        augroup AutoSaveFolds
            autocmd!
            autocmd BufWinLeave * silent! mkview
            autocmd BufWinEnter * silent! loadview
        augroup END

        " Modified from https://coderwall.com/p/usd_cw/a-pretty-vim-foldtext-function
        set foldtext=FoldText()
        function! FoldText()
            let l:lpadding = &fdc
            redir => l:signs
            execute 'silent sign place buffer='.bufnr('%')
            redir End
            let l:lpadding += l:signs =~ 'id=' ? 2 : 0

            if exists("+relativenumber")
                if (&number)
                    let l:lpadding += max([&numberwidth, strlen(line('$'))]) + 1
                elseif (&relativenumber)
                    let l:lpadding += max([&numberwidth, strlen(v:foldstart - line('w0')), strlen(line('w$') - v:foldstart), strlen(v:foldstart)]) + 1
                endif
            else
                if (&number)
                    let l:lpadding += max([&numberwidth, strlen(line('$'))]) + 1
                endif
            endif

            " expand tabs
            let l:start = substitute(getline(v:foldstart), '\t', repeat(' ', &tabstop), 'g')
            let l:end = substitute(substitute(getline(v:foldend), '\t', repeat(' ', &tabstop), 'g'), '^\s*', '', 'g')

            let l:info = '  (' . (v:foldend - v:foldstart) . ')'
            let l:infolen = strlen(substitute(l:info, '.', 'x', 'g'))
            let l:width = winwidth(0) - l:lpadding - l:infolen

            let l:separator = '...'
            let l:separatorlen = strlen(substitute(l:separator, '.', 'x', 'g'))
            let l:start = strpart(l:start , 0, l:width - strlen(substitute(l:end, '.', 'x', 'g')) - l:separatorlen)
            let l:text = l:start . l:separator . l:end

            return l:text . l:info . repeat(' ', l:width - strlen(substitute(l:text, ".", "x", "g")))
        endfunction
    endif
endif
