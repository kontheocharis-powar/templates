# system_packages:
#   - neovim

p.install({
    'init.vim': '~/.config/nvim/init.vim',
    'coc-settings.json': '~/.config/nvim/coc-settings.json',
    'nvimgit': '~/.scripts/nvim/nvimgit',
    'lightline_solarized8.vim': '~/.config/nvim/autoload/lightline/colorscheme/solarized8.vim',
})
