scripts = ('colorscheme', 'has_space')
bin_scripts = ('has-space-rs', )
path_scripts = ('colorscheme', )
subdir = 'powar'

p.install({ x: f'~/.scripts/{subdir}/{x}' for x in scripts})
p.install_bin({ x: f'~/.scripts/{subdir}/{x}' for x in bin_scripts})
p.link({f'~/.scripts/{subdir}/{s}' : f'~/.scripts/path/{s}' for s in path_scripts})
