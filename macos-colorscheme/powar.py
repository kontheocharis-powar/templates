decision = 'yes' if p.opts['color_mode'] == 'dark' else 'no'
p.execute(f'osascript -e \'tell app "System Events" to tell appearance preferences to set dark mode to {decision}\'')
