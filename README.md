# powar templates for various dotfiles

This repo contains [powar](https://github.com/kontheocharis/powar) modules for various programs and their dotfiles.

**Disclaimer**: Use this at your own risk, I am not liable for any data loss that occurs through the usage of this repo! *Please* back up your data.

This repo is highly customised to my use cases, mostly undocumented, and as such it is highly advised to fork this repo if you want to use it.
Don't be afraid to delve into the scripts and modify them!

## Instructions on how to use:

1. First, install powar using `pip3`.
2. Then, create a file `~/.config/powar-templates/global.py` with the following contents:

    ```python
    import yaml

    # Here you can choose which modules to enable:
    p.modules(
        "kitty",
        "ultra",
        "macos-colorscheme",
        "nvim",
        "yabai",
        "notion_scheduler",
        "shell",
        "scripts-unix",
        "scripts-powar",
        "scripts-uni",
        "scripts-macos",
    )

    # Only needed if you want the "nvim" module
    p.opts['nvim'] = {
        'node_host': '/opt/homebrew/bin/neovim-node-host',
        'python3_host': '/opt/homebrew/bin/python3',
        'fzf_path': '/opt/homebrew/bin/fzf',
    }

    # Next two only needed if you want color schemes

    # contains either "dark" or "light"; i.e. the current color scheme:
    p.opts['color_mode'] = p.read('~/.local/share/toggles/colorscheme').strip() 

    # contains the color scheme you want to use
    p.opts['colors'] = yaml.safe_load(
        p.read(f'~/.local/share/colorschemes/{p.opts["color_mode"]}.yml'))

    # An example colorscheme file ~/.local/share/colorschemes/{light|dark}.yml
    # contains the following:
    #
    # color15: 'f0f0f0'
    # color14: '9a9a9a'
    # color13: '6c71c4'
    # color12: '8c8c8c'
    # color11: '747474'
    # color10: '666666'
    # color9: 'cb4b16'
    # color8: '1b1b1b'
    # color7: 'e1e1e1'
    # color6: '2aa198'
    # color5: 'd33682'
    # color4: '268bd2'
    # color3: 'b58900'
    # color2: '859900'
    # color1: 'dc322f'
    # color0: '242424'
    # cursor: '8c8c8c'
    # foreground: '8c8c8c'
    # background: '1b1b1b'

    # Only needed if you want the "kitty" module
    p.opts['kitty'] = {
        'font_main': 'RobotoMono Nerd Font',
        'font_bold_suffix': 'Bold',
        'font_italic_suffix': 'Italic',
    }
    ```
3. Clone this repo inside `~/.config/powar-config`
4. Run `powar install` to install the modules based on the global config (seen above).
5. If you want the "ultra" module (system-wide vim keys for MacOS):

    1. You need to install:
        - Hammerspoon
        - Yabai
        - Karabiner Elements
    2. Install the Hammerspoon CLI, see <https://www.hammerspoon.org/docs/hs.ipc.html> for info.
    3. Run `powar install`, ensuring that "ultra" is present in `p.modules` in the global config. This should populate Karabiner and Hammerspoon configs.
    4. Enable the "Ultra" complex customisation in Karabiner Elements.
    5. In your Hammerspoon config, add `hs.loadSpoon("Ultra")`, and reload config.
    6. A menu bar item should be present that shows your current vim mode. Usual vim keys work on text fields if you are in normal mode ("N").
    7. To customise the keybindings, edit `~/.config/powar-templates/ultra/powar.py`. That contains most Karabiner-related configuration. In that folder you will also see other files related to Hammerspoon, to remember the vim mode of each window, and to interface with Karabiner to switch modes when they are selected in the menu bar. Warning, this code is undocumented, modify at your own risk!
