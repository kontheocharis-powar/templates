local Ultra = {
  author = "Constantine Theocharis",
  license = "MIT",
  name = "Ultra",
  version = "1.0.0",
}

require("hs.ipc")
hs.ipc.cliInstall()

local common = hs.json.decode('{{ local.common_json }}')
local confStr = common['karabiner_variable_names']

local Mode = {}
for i, name in ipairs(common.modes) do
  Mode[string.upper(name)] = i - 1
end

local Layer = {}
for i, name in ipairs(common.layers) do
  Layer[string.upper(name)] = i - 1
end

local applications = {}
local applicationModes = {}
for i, entry in ipairs(common["application_modes"]) do
  applications[entry[1]] = i - 1
  applicationModes[i - 1] = Mode[string.upper(entry[3])]
end
local noneIndex = applications["NONE"]
local noneMode = applicationModes[noneIndex]

local ultraModeBar = hs.menubar.new(true)
local images = {
  [Mode.NORMAL] = hs.image.imageFromPath("{{ local.spoon_path }}/menubar-shapes/normal.pdf"),
  [Mode.INSERT] = hs.image.imageFromPath("{{ local.spoon_path }}/menubar-shapes/insert.pdf"),
  [Mode.PASSTHROUGH] = hs.image.imageFromPath("{{ local.spoon_path }}/menubar-shapes/passthrough.pdf"),
  [Mode.NATIVE] = hs.image.imageFromPath("{{ local.spoon_path }}/menubar-shapes/passthrough.pdf"),
  [Mode.VISUAL] = hs.image.imageFromPath("{{ local.spoon_path }}/menubar-shapes/visual.pdf"),
  [Mode.SEARCH] = hs.image.imageFromPath("{{ local.spoon_path }}/menubar-shapes/search.pdf"),
}

for _, im in pairs(images) do
  im:size({w=16,h=16})
end

local kCliPath = "/Library/Application Support/org.pqrs/Karabiner-Elements/bin/karabiner_cli"

function setVariable(name, value)
  hs.execute(string.format("'%s' --set-variables '{\"%s\": %d}'", kCliPath, name, value))
end

local debugAppName = false

local recentModes = {}
local current = nil

function setModeFor(applicationName)
  applicationIndex = applications[applicationName]
  if applicationIndex ~= nil then
    setVariable(confStr['application'], applicationIndex)
  else
    setVariable(confStr['application'], noneIndex)
  end

  if recentModes[applicationName] ~= nil then
    setMode(recentModes[applicationName])
  else
    if applicationIndex ~= nil then
      setMode(applicationModes[applicationIndex])
    else
      setMode(noneMode)
    end
  end
end

local watcher =
  hs.application.watcher.new(function(applicationName, eventType, application)
    if eventType == hs.application.watcher.activated then
      if debugAppName then
        hs.alert(applicationName)
      end
      current = applicationName
      setModeFor(applicationName)
    end
  end)

local ultraMode = nil

function setModeBarMenu()
  local modeBarTable = {}
  for modeStr, mode in pairs(Mode) do
    table.insert(modeBarTable, {title = modeStr, checked = (ultraMode == mode), fn = function() Ultra:setMode(mode) end})
  end
  table.insert(modeBarTable, {title = "-"})

  for _, option in ipairs({'username', 'password', 'both'}) do
    table.insert(modeBarTable, {title = string.format("pass: %s", option), fn = function()
      Ultra:setMode(Mode.SEARCH)
      Ultra:passDialog(option)
    end})
  end

  ultraModeBar:setMenu(modeBarTable)
end

function setMode(mode)
  ultraModeBar:setIcon(images[mode], mode == Mode.NORMAL)
  setVariable(confStr['mode'], mode)
  ultraMode = mode
  setModeBarMenu()
end

function Ultra:setMode(mode)
  if current ~= nil then
    recentModes[current] = mode
  end
  setMode(mode)
end

function Ultra:getMode()
  return ultraMode
end

mouseModeSwitchTap = hs.eventtap.new({
  hs.eventtap.event.types.leftMouseDown,
  hs.eventtap.event.types.rightMouseDown
}, function(event)
  local currentMode = Ultra:getMode()
  if currentMode == Mode.NORMAL or currentMode == Mode.VISUAL then
    Ultra:setMode(Mode.INSERT)
  end
  return false, {}
end)
function setupMouseModeSwitch()
  mouseModeSwitchTap:start()
end


local lastPassChoice = nil

-- mode: 'password' | 'username' | 'both'
function Ultra:passDialog(mode)
  files, fStatus, _, _ = hs.execute('~/.scripts/passutility files', true)

  if fStatus == nil then
    hs.alert('pass list returned with non-zero exit code.')
    return
  end

  lines = {}
  for s in files:gmatch("[^\r\n]+") do
      if lastPassChoice == s then
        table.insert(lines, 1, {text = s, subText = "Last used"})
      else
        table.insert(lines, {text = s})
      end
  end

  c = hs.chooser.new(function(selected)
    if mode == 'password' or mode == 'both' then
      password, pStatus, _, _ = hs.execute(string.format('~/.scripts/passutility get-password %s', selected.text), true)
      if pStatus == nil then
        hs.alert('pass password returned with non-zero exit code.')
        return
      end
    end
    if mode == 'username' or mode == 'both' then
      username, uStatus, _, _ = hs.execute(string.format('~/.scripts/passutility get-username %s', selected.text), true)
      if uStatus == nil then
        hs.alert('pass username returned with non-zero exit code.')
        return
      end
    end

    lastPassChoice = selected.text

    if password ~= nil then
      password = string.gsub(password, '\n', '')
    end

    if username ~= nil then
      username = string.gsub(username, '\n', '')
    end

    if mode == 'password' then
      hs.eventtap.keyStrokes(password)
    elseif mode == 'username' then
      hs.eventtap.keyStrokes(username)
    elseif mode == 'both' then
      hs.eventtap.keyStrokes(username)
      hs.eventtap.keyStroke({}, 'tab')
      hs.eventtap.keyStrokes(password)
    end
  end)
  c:choices(lines)
  c:show()
end

function Ultra:enableDebugAppName()
  debugAppName = true
end

function Ultra:disableDebugAppName()
  debugAppName = false
end

function Ultra:init()
  watcher:start()
  current = hs.application.frontmostApplication():name()
  setModeFor(current)
  setupMouseModeSwitch()
end

return Ultra
