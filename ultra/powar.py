# type: ignore
from __future__ import annotations
import json
from dataclasses import dataclass, field
from typing import Tuple, List, Dict, Optional, Union, Any, FrozenSet
import os
import enum
import logging
import abc
from functools import reduce
import operator
from collections.abc import Iterable

logger: logging.Logger = logging.getLogger(__name__)


def realpath(path: str) -> str:
    return os.path.expandvars(os.path.expanduser(path))


common = json.load(open('common.json'))
conf_str = common['karabiner_variable_names']

Mode = enum.IntEnum(
    'Mode',
    {name.upper(): i
     for i, name in enumerate(common['modes'])},
)
Specifier = enum.IntEnum(
    'Specifier',
    {name.upper(): i
     for i, name in enumerate(common['specifiers'])},
)
Layer = enum.IntEnum(
    'Layer',
    {name.upper(): i
     for i, name in enumerate(common['layers'])},
)
Operator = enum.IntEnum(
    'Operator',
    {name.upper(): i
     for i, name in enumerate(common['operators'])},
)
Application = enum.IntEnum(
    'Application',
    {
        name.upper(): i
        for i, (real_name, name,
                mode) in enumerate(common['application_modes'])
    },
)
application_name = {
    i: real_name
    for i, (real_name, _, _) in enumerate(common['application_modes'])
}

LAYER_MOD = 'left_command'


class BindingType(abc.ABC):
    pass


@dataclass(init=False, unsafe_hash=True)
class Binding(BindingType):
    key: str
    modifiers: FrozenSet[str]
    specifier: Specifier
    mouse: bool

    def __init__(self,
                 key: str,
                 modifiers: FrozenSet[str] = frozenset(),
                 specifier: Specifier = Specifier.NONE,
                 mouse: bool = False):
        self.key = key
        self.modifiers = frozenset(modifiers)
        self.specifier = specifier
        self.mouse = mouse

    def end_of_line() -> Binding:
        return Binding('right_arrow', {'right_command'})

    def begin_of_line() -> Binding:
        return Binding('left_arrow', {'right_command'})

    def word_forward() -> Binding:
        return Binding('right_arrow', {'left_alt'})

    def word_back() -> Binding:
        return Binding('left_arrow', {'left_alt'})

    def cut() -> Binding:
        return Binding('x', {'right_command'})

    def paste() -> Binding:
        return Binding('v', {'right_command'})

    def copy() -> Binding:
        return Binding('c', {'right_command'})

    def up() -> Binding:
        return Binding('up_arrow')

    def down() -> Binding:
        return Binding('down_arrow')

    def left() -> Binding:
        return Binding('left_arrow')

    def right() -> Binding:
        return Binding('right_arrow')

    def page_up() -> Binding:
        return Binding('page_up')

    def page_down() -> Binding:
        return Binding('page_down')

    def page_top() -> Binding:
        return Binding('home')

    def page_bottom() -> Binding:
        return Binding('end')

    def search() -> Binding:
        return Binding('f', {'right_command'})

    def next_occurence() -> Binding:
        return Binding('g', {'right_command'})

    def prev_occurence() -> Binding:
        return Binding('g', {'right_command', 'left_shift'})

    def undo() -> Binding:
        return Binding('z', {'right_command'})

    def redo() -> Binding:
        return Binding('z', {'right_command', 'left_shift'})

    def zoom_in() -> Binding:
        return Binding('equal_sign', {'right_command'})

    def zoom_out() -> Binding:
        return Binding('hyphen', {'right_command'})

    def zoom_reset() -> Binding:
        return Binding('0', {'right_command'})

    def zoom_fit() -> Binding:
        return Binding('9', {'right_command'})

    def close_window() -> Binding:
        return Binding('w', {'right_command'})

    def quit() -> Binding:
        return Binding('q', {'right_command'})

    def spotlight() -> Binding:
        return Binding('spacebar', {'right_command'})

    def noop() -> Binding:
        return Binding('vk_none')

    def add_mod(self, mod: str):
        return self.__class__(self.key, self.modifiers.union(frozenset({mod})))

    def add_mods(self, mods: Set[str]):
        return self.__class__(self.key, self.modifiers.union(frozenset(mod)))

    def add_shift(self):
        return self.add_mod('left_shift')

    def add_control(self):
        return self.add_mod('control')


@dataclass(init=False, unsafe_hash=True)
class LayerBinding(Binding):
    pass


@dataclass(init=False)
class Command(BindingType):
    value: str

    def __init__(self, value: str):
        self.value = f"TERM=xterm /bin/zsh -l -c {json.dumps(value)}"

    def focus_window(direction: str) -> Command:
        return Command(f'yabai -m window --focus {direction}')

    def move_window(direction: str) -> Command:
        return Command(f'yabai -m window --warp {direction}')

    def resize_window(resize_command: str) -> Command:
        return Command(f'yabai -m window --resize {resize_command}')

    def focus_space(direction: str) -> BindingType:
        if yabai_scripting_addon:
            return Command(
                f'~/.scripts/powar/has-space-rs {direction} && yabai -m space --focus {direction}'
            )
        else:
            return Binding(
                'right_arrow' if direction == 'next' else 'left_arrow', {'left_control'})

    def focus_display(direction: str) -> Command:
        return Command(f'yabai -m display --focus {direction}')

    def move_to_space(direction: str) -> Command:
        return Command(
            f'~/.scripts/powar/has-space-rs {direction} && yabai -m window --space {direction}'
        )

    def move_to_display(direction: str) -> Command:
        return Command(f'yabai -m window --display {direction}')

    def close_window() -> Command:
        return Command(f'yabai -m window --close')

    def toggle_float() -> Command:
        return Command(
            f'yabai -m window --toggle float; yabai -m window --grid 6:4:1:1:2:4'
        )

    def toggle_zen() -> Command:
        return Command(
            f'yabai -m window --toggle float; yabai -m window --grid 1:4:1:1:2:1'
        )

    def toggle_fullscreen() -> Command:
        return Command(f'yabai -m window --toggle native-fullscreen')

    def toggle_colorscheme() -> Command:
        return Command(f'~/.scripts/powar/colorscheme toggle')

    def hammerspoon(command: str) -> Command:
        return Command(f"hs -c {json.dumps(command)}")

    def applescript(command: str) -> Command:
        return Command(f"osascript -e {json.dumps(command)}")


@dataclass
class SetMode(BindingType):
    mode: Mode


@dataclass
class SetOperator(BindingType):
    operator: Operator


@dataclass
class SetLayer(BindingType):
    layer: Layer


@dataclass
class SetSpecifier(BindingType):
    specifier: Specifier


@dataclass(unsafe_hash=True)
class Context:
    mode: Optional[Mode] = None
    not_mode: Optional[Union[Mode, Iterable[Mode]]] = Mode.PASSTHROUGH
    operator: Optional[Operator] = Operator.NORMAL
    layer: Optional[Layer] = Layer.NONE
    application: Optional[Application] = None
    specifier: Optional[Specifier] = Specifier.NONE


ModeMaps = Dict[Context, Union[List[BindingType], BindingType]]

hs_path = p.render("{{ultra.hs_path}}")
yabai_scripting_addon = True if p.render(
    "{{ultra.yabai_scripting_addon}}") == "True" else False


class Manipulators:
    manipulators = []

    def __init__(self, entries: Dict[Union[Binding, DoubleBinding], ModeMaps]):
        self.add_many(entries)

    def add_many(self, entries: Dict[Union[Binding, DoubleBinding], ModeMaps]):
        for from_binding, mode_maps in entries.items():
            self.add(from_binding, mode_maps)

    def add(self, from_binding: Union[Binding, DoubleBinding],
            mode_maps: ModeMaps):
        for context, binding_type in mode_maps.items():
            if from_binding.mouse:
                entry = {
                    'type': 'basic',
                    'from': {
                        'pointing_button': from_binding.key
                    }
                }
            else:
                entry = {
                    'type': 'basic',
                    'from': {
                        'key_code': from_binding.key
                    }
                }

            if isinstance(from_binding, LayerBinding):
                entry['from'].setdefault('modifiers',
                                         {}).setdefault('mandatory',
                                                        []).append(LAYER_MOD)

            conditions = self._handle_context(context)
            if conditions:
                entry['conditions'] = conditions

            if from_binding.modifiers:
                entry['from'].setdefault('modifiers', {}).setdefault(
                    'mandatory', []).extend(list(from_binding.modifiers))

            entry = {**entry, **self._handle_to_binding(binding_type)}
            self.manipulators.append(entry)

    def _handle_context(self, context: Context) -> List[Dict[Any, Any]]:
        conditions = []

        def handle_mode(mode):
            return {
                "type": "variable_if",
                "name": conf_str['mode'],
                "value": mode,
            }

        def handle_not_mode(mode):
            return {
                "type": "variable_unless",
                "name": conf_str['mode'],
                "value": mode,
            }

        def handle_operator(op):
            return {
                "type": "variable_if",
                "name": conf_str['operator'],
                "value": op,
            }

        def handle_layer(layer):
            return {
                "type": "variable_if",
                "name": conf_str['layer'],
                "value": layer,
            }

        def handle_application(app):
            return {
                "type": "variable_if",
                "name": conf_str['application'],
                "value": app,
            }

        def handle_application(app):
            return {
                "type": "variable_if",
                "name": conf_str['application'],
                "value": app,
            }

        def handle_specifier(app):
            return {
                "type": "variable_if",
                "name": conf_str['specifier'],
                "value": app,
            }

        for entry, cla, handle in (
            (context.mode, Mode, handle_mode),
            (context.not_mode, Mode, handle_not_mode),
            (context.operator, Operator, handle_operator),
            (context.layer, Layer, handle_layer),
            (context.application, Application, handle_application),
            (context.specifier, Specifier, handle_specifier),
        ):
            if entry is not None:
                if isinstance(entry, cla):
                    conditions.append(handle(entry))
                elif isinstance(entry, Iterable):
                    conditions.extend([handle(x) for x in entry])

        return conditions

    def _handle_to_binding(
        self,
        to: Union[BindingType, List[BindingType]],
    ) -> Dict[Any, Any]:

        def hs(code):
            return f"{hs_path} -c '{code}'"

        result = {}
        bt = [to] if isinstance(to, BindingType) else to
        for val in bt:
            if isinstance(val, Binding):
                if val.mouse:
                    entry = {
                        "pointing_button": val.key,
                    }
                else:
                    entry = {
                        "key_code": val.key,
                    }
                if val.modifiers:
                    entry['modifiers'] = list(val.modifiers)
                result.setdefault('to', []).append(entry)
            elif isinstance(val, Command):
                result.setdefault('to', []).append({
                    "shell_command": val.value,
                })
            elif isinstance(val, SetMode):
                result.setdefault('to', []).append({
                    "shell_command":
                    hs(f'spoon.Ultra:setMode({val.mode.value})'),
                })
            elif isinstance(val, SetOperator):
                result.setdefault('to', []).append({
                    "set_variable": {
                        "name": conf_str['operator'],
                        "value": val.operator,
                    }
                })
            elif isinstance(val, SetSpecifier):
                result.setdefault('to', []).append({
                    "set_variable": {
                        "name": conf_str['specifier'],
                        "value": val.specifier,
                    }
                })
                result.setdefault('to_delayed_action',
                                  {}).setdefault('to_if_invoked', []).append({
                                      "set_variable": {
                                          "name": conf_str['specifier'],
                                          "value": Specifier.NONE,
                                      }
                                  })
            elif isinstance(val, SetLayer):
                result.setdefault('to', []).extend([{
                    "key_code": "vk_none"
                }, {
                    "set_variable": {
                        "name": conf_str['layer'],
                        "value": val.layer,
                    }
                }])
                result.setdefault('to_after_key_up', []).extend([{
                    "key_code":
                    "vk_none"
                }, {
                    "set_variable": {
                        "name": conf_str['layer'],
                        "value": Layer.NONE,
                    }
                }])
            else:
                assert 0
        return result


m = Manipulators({
    Binding('right_shift'): {
        Context(not_mode=(Mode.PASSTHROUGH, Mode.NATIVE, Mode.NORMAL, Mode.SEARCH),
                operator=None,
                layer=None,
                specifier=None): [
            SetLayer(Layer.NONE),
            SetSpecifier(Specifier.NONE),
            SetOperator(Operator.NORMAL),
            SetMode(Mode.NORMAL),
        ],
        Context(mode=Mode.NORMAL, operator=None, layer=None, specifier=None): [
            SetLayer(Layer.NONE),
            SetSpecifier(Specifier.NONE),
            SetOperator(Operator.NORMAL),
            Binding('escape'),
        ],
        Context(mode=Mode.SEARCH, operator=None, layer=None, specifier=None): [
            SetLayer(Layer.NONE),
            SetSpecifier(Specifier.NONE),
            SetOperator(Operator.NORMAL),
            SetMode(Mode.NORMAL),
            Binding('escape'),
        ],
        Context(mode=Mode.NATIVE): [
            Binding('escape'),
        ],
    },
    Binding('right_shift', {'left_shift'}): {
        Context(not_mode=(Mode.NATIVE),
                operator=None,
                layer=None,
                specifier=None): [
            SetLayer(Layer.NONE),
            SetSpecifier(Specifier.NONE),
            SetOperator(Operator.NORMAL),
            SetMode(Mode.NATIVE),
        ],
        Context(Mode.NATIVE): [
            SetLayer(Layer.NONE),
            SetSpecifier(Specifier.NONE),
            SetOperator(Operator.NORMAL),
            SetMode(Mode.NORMAL),
        ],
        Context(Mode.PASSTHROUGH,
                not_mode=None,
                operator=None,
                layer=None,
                specifier=None): [
            SetLayer(Layer.NONE),
            SetSpecifier(Specifier.NONE),
            SetOperator(Operator.NORMAL),
            SetMode(Mode.NORMAL),
        ],
    },
    Binding('z'): {
        Context(Mode.NORMAL): [
            SetMode(Mode.SEARCH),
            Binding('button1', mouse=True),
        ],
    },
    Binding('x'): {
        Context(Mode.NORMAL): [
            Binding.right().add_shift(),
            Binding.cut(),
        ],
        Context(Mode.VISUAL): [Binding.cut(),
                               SetMode(Mode.NORMAL)],
    },
    Binding('x', {'left_shift'}): {
        Context(Mode.NORMAL): [Binding('delete_or_backspace', {'command'})],
    },
    Binding('c'): {
        Context(Mode.NORMAL):
        SetOperator(Operator.CHANGE),
        Context(Mode.VISUAL): [
            Binding.cut(),
            SetMode(Mode.INSERT),
        ],
        Context(Mode.NORMAL, operator=Operator.CHANGE): [
            Binding.begin_of_line(),
            Binding.end_of_line().add_shift(),
            Binding.cut(),
            SetOperator(Operator.NORMAL),
            SetMode(Mode.INSERT),
        ],
    },
    Binding('c', {'shift'}): {
        Context(Mode.NORMAL): [
            Binding.end_of_line().add_shift(),
            Binding.cut(),
            SetMode(Mode.INSERT)
        ],
    },
    Binding('i'): {
        Context(Mode.NORMAL): SetMode(Mode.INSERT),
        Context(Mode.NORMAL, operator=Operator.CHANGE):
        SetSpecifier(Specifier.IN),
        Context(Mode.NORMAL, operator=Operator.DELETE):
        SetSpecifier(Specifier.IN),
        Context(Mode.NORMAL, operator=Operator.YANK):
        SetSpecifier(Specifier.IN),
        Context(Mode.VISUAL): SetSpecifier(Specifier.IN),
    },
    Binding('w'): {
        Context(Mode.NORMAL, operator=Operator.CHANGE, specifier=Specifier.IN):
        [
            Binding.word_back(),
            Binding.word_forward().add_shift(),
            SetSpecifier(Specifier.NONE),
            Binding.cut(),
            SetOperator(Operator.NORMAL),
            SetMode(Mode.INSERT),
        ],
        Context(Mode.NORMAL, operator=Operator.DELETE, specifier=Specifier.IN):
        [
            Binding.word_back(),
            Binding.word_forward().add_shift(),
            SetSpecifier(Specifier.NONE),
            Binding.cut(),
            SetOperator(Operator.NORMAL),
        ],
        Context(Mode.NORMAL, operator=Operator.YANK, specifier=Specifier.IN): [
            Binding.word_back(),
            Binding.word_forward().add_shift(),
            SetSpecifier(Specifier.NONE),
            Binding.copy(),
            SetOperator(Operator.NORMAL),
        ],
        Context(Mode.VISUAL, specifier=Specifier.IN): [
            Binding.word_back(),
            Binding.word_forward().add_shift(),
            SetSpecifier(Specifier.NONE),
        ],
        Context(Mode.NORMAL,
                operator=Operator.CHANGE,
                specifier=Specifier.AROUND): [
            Binding.word_back(),
            Binding.left(),
            Binding.word_forward().add_shift(),
            SetSpecifier(Specifier.NONE),
            Binding.cut(),
            SetOperator(Operator.NORMAL),
            SetMode(Mode.INSERT),
        ],
        Context(Mode.NORMAL,
                operator=Operator.DELETE,
                specifier=Specifier.AROUND): [
            Binding.word_back(),
            Binding.left(),
            Binding.word_forward().add_shift(),
            SetSpecifier(Specifier.NONE),
            Binding.cut(),
            SetOperator(Operator.NORMAL),
        ],
        Context(Mode.NORMAL,
                operator=Operator.YANK,
                specifier=Specifier.AROUND): [
            Binding.word_back(),
            Binding.left(),
            Binding.word_forward().add_shift(),
            SetSpecifier(Specifier.NONE),
            Binding.copy(),
            SetOperator(Operator.NORMAL),
        ],
        Context(Mode.VISUAL, specifier=Specifier.AROUND): [
            Binding.word_back(),
            Binding.left(),
            Binding.word_forward().add_shift(),
            SetSpecifier(Specifier.NONE),
        ],
    },
    Binding('v'): {
        Context(Mode.NORMAL): SetMode(Mode.VISUAL),
    },
    Binding('v', {'left_shift'}): {
        Context(Mode.NORMAL): [
            Binding.begin_of_line(),
            Binding.end_of_line().add_shift(),
            SetMode(Mode.VISUAL)
        ],
    },
    Binding('o'): {
        Context(Mode.NORMAL): [
            Binding.end_of_line(),
            Binding('return_or_enter'),
            SetMode(Mode.INSERT)
        ],
    },
    Binding('o', {'left_shift'}): {
        Context(Mode.NORMAL): [
            Binding.begin_of_line(),
            Binding('return_or_enter'),
            Binding.up(),
            SetMode(Mode.INSERT)
        ],
    },
    Binding('a'): {
        Context(Mode.NORMAL): [
            Binding.right(),
            SetMode(Mode.INSERT),
        ],
        Context(Mode.NORMAL, operator=Operator.CHANGE):
        SetSpecifier(Specifier.AROUND),
        Context(Mode.NORMAL, operator=Operator.DELETE):
        SetSpecifier(Specifier.AROUND),
        Context(Mode.NORMAL, operator=Operator.YANK):
        SetSpecifier(Specifier.AROUND),
        Context(Mode.VISUAL):
        SetSpecifier(Specifier.AROUND),
    },
    Binding('i', {'left_shift'}): {
        Context(Mode.NORMAL): [
            Binding.begin_of_line(),
            SetMode(Mode.INSERT),
        ],
    },
    Binding('a', {'left_shift'}): {
        Context(Mode.NORMAL): [
            Binding.end_of_line(),
            SetMode(Mode.INSERT),
        ],
    },
    Binding('u'): {
        Context(Mode.NORMAL): Binding.undo(),
    },
    Binding('r', {'control'}): {
        Context(Mode.NORMAL): Binding.redo(),
    },
    Binding('p'): {
        Context(Mode.NORMAL): Binding.paste(),
        Context(Mode.VISUAL): [
            Binding.paste(),
            SetMode(Mode.NORMAL),
        ],
    },
    Binding('slash'): {
        Context(Mode.NORMAL): [
            Binding.search(),
            SetMode(Mode.SEARCH),
        ]
    },
    Binding('return_or_enter'): {
        Context(Mode.SEARCH): [
            Binding('return_or_enter'),
            SetMode(Mode.NORMAL),
        ]
    },
    Binding('return_or_enter', {'command'}): {
        Context(Mode.INSERT): [
            Binding('return_or_enter'),
            SetMode(Mode.NORMAL),
        ],
        Context(Mode.SEARCH): [
            Binding('return_or_enter'),
            SetMode(Mode.NORMAL),
        ]
    },
    Binding('escape'): {
        Context(Mode.SEARCH): [
            Binding('escape'),
            SetMode(Mode.NORMAL),
        ]
    },
    Binding('n'): {
        Context(Mode.NORMAL): [
            Binding.next_occurence(),
        ]
    },
    Binding('n', {'left_shift'}): {
        Context(Mode.NORMAL): [
            Binding.prev_occurence(),
        ]
    },
    Binding('equal_sign'): {
        Context(Mode.NORMAL): Binding.zoom_reset()
    },
    Binding('equal_sign', {'left_shift'}): {
        Context(Mode.NORMAL): Binding.zoom_in()
    },
    Binding('hyphen'): {
        Context(Mode.NORMAL): Binding.zoom_out()
    },
    Binding('s'): {
        Context(Mode.NORMAL): Binding.zoom_fit()
    },
    # Binding('w', {'command'}): {
    #     Context(not_mode=Mode.PASSTHROUGH, operator=None, specifier=None):
    #     Binding.noop(),
    # },
    # Binding('q', {'command'}): {
    #     Context(not_mode=Mode.PASSTHROUGH, operator=None, specifier=None):
    #     Binding.noop(),
    # },
    Binding('a', {LAYER_MOD}): {
        Context(): SetLayer(Layer.WINDOW),
    },
    Binding('s', {LAYER_MOD}): {
        Context(): SetLayer(Layer.APPLICATION),
    },
    Binding('r', {LAYER_MOD}): {
        Context(): SetLayer(Layer.RESIZE),
    },
    Binding('d', {LAYER_MOD}): {
        Context(): SetLayer(Layer.MOVE),
    },
})

# pass layer
# for key, option in (('u', 'username'), ('p', 'password'), ('b', 'both')):
#     m.add(
#         LayerBinding(key), {
#             Context(layer=Layer.PASSWORD, mode=Mode.NORMAL): [
#                 SetMode(Mode.SEARCH),
#                 Command.hammerspoon(f'spoon.Ultra:passDialog("{option}")')
#             ],
#             Context(layer=Layer.PASSWORD, mode=Mode.INSERT):
#             Command.hammerspoon(f'spoon.Ultra:passDialog("{option}")'),
#         })

# window layer
m.add_many({
    LayerBinding('d'): {
        Context(layer=Layer.WINDOW): Command.close_window(),
    },
    LayerBinding('q'): {
        Context(layer=Layer.WINDOW): Binding.quit(),
    },
    LayerBinding('n'): {
        Context(layer=Layer.WINDOW): Binding('n', {'command'}),
    },
    LayerBinding('s'): {
        Context(layer=Layer.WINDOW): Binding('s', {'command'}),
    },
    LayerBinding('y'): {
        Context(layer=Layer.WINDOW): Binding('d', {'command'}),
    },
    LayerBinding('m'): {
        Context(layer=Layer.WINDOW): Command.toggle_colorscheme(),
    },
    LayerBinding('g'): {
        Context(layer=Layer.WINDOW): Command('~/.scripts/macos/swapspaces'),
    },
    LayerBinding('8'): {
        Context(layer=Layer.WINDOW): Binding('a', {'command'}),
    },
    LayerBinding('f'): {
        Context(layer=Layer.WINDOW): Command.toggle_fullscreen(),
    },
    LayerBinding('c'): {
        Context(layer=Layer.WINDOW): Command.toggle_float(),
    },
    LayerBinding('v'): {
        Context(layer=Layer.WINDOW): Command.toggle_zen(),
    },
    LayerBinding('slash'): {
        Context(layer=Layer.WINDOW): Binding('f12'), # toggle input source
    },
    LayerBinding('return_or_enter'): {
        Context(layer=Layer.WINDOW): Command('~/.scripts/macos/terminal'),
    },
    LayerBinding('spacebar'): { # bind cmd-f12 to spotlight/alfred for this to work
        Context(layer=Layer.WINDOW): Binding('f12', {'command'}),
    },
})

m.add_many({
    Binding('button4', {'command'}, mouse=True): {
        Context(not_mode=(Mode.PASSTHROUGH),
                operator=None,
                layer=None,
                specifier=None):
        Command.focus_space('prev')
    },
    Binding('button5', {'command'}, mouse=True): {
        Context(not_mode=(Mode.PASSTHROUGH),
                operator=None,
                layer=None,
                specifier=None):
        Command.focus_space('next')
    },
})

for key, direction, resize_command in (
    ('h', 'west', 'right:-20:0'),
    ('j', 'south', 'bottom:0:20'),
    ('k', 'north', 'bottom:0:-20'),
    ('l', 'east', 'right:20:0'),
):
    m.add(
        LayerBinding(key),
        {Context(layer=Layer.WINDOW): Command.focus_window(direction)},
    )
    m.add(
        LayerBinding(key),
        {Context(layer=Layer.MOVE): Command.move_window(direction)},
    )
    m.add(
        LayerBinding(key),
        {Context(layer=Layer.RESIZE): Command.resize_window(resize_command)},
    )

for key, direction in (
    ('u', 'prev'),
    ('p', 'next'),
):
    m.add(
        LayerBinding(key),
        {Context(layer=Layer.WINDOW): Command.focus_space(direction)},
    )
    m.add(
        LayerBinding(key),
        {Context(layer=Layer.MOVE): Command.move_to_space(direction)},
    )

for key, direction in (
    ('i', 'prev'),
    ('o', 'next'),
):
    m.add(
        LayerBinding(key),
        {Context(layer=Layer.WINDOW): Command.focus_display(direction)},
    )
    m.add(
        LayerBinding(key),
        {Context(layer=Layer.MOVE): Command.move_to_display(direction)},
    )

# web browser layer
for a in (Application.BRAVE, Application.SAFARI, Application.CHROMIUM,
          Application.FIREFOX, Application.CHROME):
    browser = Context(layer=Layer.APPLICATION, application=a)

    focus_input_js = """
    var el = document.querySelectorAll("input:not([type=\\"hidden\\"])")[0];
    el.focus();
    el.setSelectionRange(1e9,1e9);
    """

    def exec_js(js_str: str) -> Command:
        return Command.applescript(
            f'tell application {json.dumps(application_name[a])} to execute (active tab of window 1) javascript {json.dumps(js_str)}'
        )

    m.add_many({
        LayerBinding('j'): {
            browser: Binding('open_bracket', {'left_shift', 'command'})
        },
        LayerBinding('k'): {
            browser: Binding('close_bracket', {'left_shift', 'command'})
        },
        LayerBinding('h'): {
            browser: Binding('open_bracket', {'command'})
        },
        LayerBinding('l'): {
            browser: Binding('close_bracket', {'command'})
        },
        LayerBinding('o'): {
            browser: [Binding('l', {'command'}),
                      SetMode(Mode.SEARCH)]
        },
        LayerBinding('t'): {
            browser: [Binding('t', {'command'}),
                      SetMode(Mode.SEARCH)]
        },
        LayerBinding('u'): {
            browser: Binding('t', {'command', 'left_shift'}),
        },
        LayerBinding('f'): {
            browser:
            [Binding('a', {'command', 'left_shift'}),
             SetMode(Mode.SEARCH)],
        },
        LayerBinding('r'): {
            browser: Binding('r', {'command'})
        },
        LayerBinding('d'): {
            browser: Binding('w', {'command'})
        },
        LayerBinding('c'): {
            browser: Binding('i', {'command', 'left_option'})
        },
        LayerBinding('i'): {
            Context(layer=Layer.APPLICATION, application=a, mode=Mode.NORMAL):
            [
                SetMode(Mode.SEARCH),
                exec_js(focus_input_js),
            ],
            Context(layer=Layer.APPLICATION, application=a, mode=Mode.INSERT):
            exec_js(focus_input_js),
        },
    })

# vscode
vscode = Context(layer=Layer.APPLICATION, application=Application.CODE)
m.add_many({
    LayerBinding('j'): {
        vscode: Binding('open_bracket', {'left_shift', 'command'})
    },
    LayerBinding('k'): {
        vscode: Binding('close_bracket', {'left_shift', 'command'})
    },
    LayerBinding('o'): {
        vscode: [Binding('p', {'left_shift', 'command'})]
    },
    LayerBinding('u'): {
        vscode: Binding('t', {'command', 'left_shift'}),
    },
})

# intellij
intellij = Context(layer=Layer.APPLICATION, application=Application.INTELLIJ)
m.add_many({
    LayerBinding('j'): {
        intellij: Binding('open_bracket', {'left_shift', 'command'})
    },
    LayerBinding('k'): {
        intellij: Binding('close_bracket', {'left_shift', 'command'})
    },
})

# notion
notion = Context(layer=Layer.APPLICATION, application=Application.NOTION)
m.add_many({
    LayerBinding('spacebar'): {
        notion: [Binding('p', {'command'}),
                 SetMode(Mode.SEARCH)]
    },
    LayerBinding('n'): {
        notion: Binding('n', {'command', 'left_shift'})
    },
    LayerBinding('i'): {
        notion: Binding('open_bracket', {'command'})
    },
    LayerBinding('o'): {
        notion: Binding('close_bracket', {'command'})
    },
    LayerBinding('j'): {
        notion: Binding('down_arrow', {'left_shift', 'command'})
    },
    LayerBinding('k'): {
        notion: Binding('up_arrow', {'left_shift', 'command'})
    },
    LayerBinding('h'): {
        notion: Binding('left_arrow', {'left_shift', 'command'})
    },
    LayerBinding('l'): {
        notion: Binding('right_arrow', {'left_shift', 'command'})
    },
    LayerBinding('z'): {
        notion: Binding('t', {'left_alt', 'command'})
    },
    LayerBinding('o'): {
        notion: Binding('close_bracket', {'command'})
    },
    # LayerBinding('spacebar'): {
    #     notion: Binding('return_or_enter', {'command'})
    # },
    LayerBinding('e'): {
        notion: [Binding('slash', {'command'}),
                 SetMode(Mode.SEARCH)]
    },
})

# discord
discord = Context(layer=Layer.APPLICATION, application=Application.DISCORD)
discordptb = Context(layer=Layer.APPLICATION,
                     application=Application.DISCORDPTB)
for disc in (discord, discordptb):
    m.add_many({
        LayerBinding('spacebar'): {
            disc: [Binding('k', {'command'}),
                   SetMode(Mode.SEARCH)]
        },
        LayerBinding('i'): {
            disc: Binding('down_arrow', {'left_alt', 'command'})
        },
        LayerBinding('o'): {
            disc: Binding('up_arrow', {'left_alt', 'command'})
        },
        LayerBinding('j'): {
            disc: Binding('down_arrow', {'left_alt'})
        },
        LayerBinding('k'): {
            disc: Binding('up_arrow', {'left_alt'})
        },
    })

# mail
mail = Context(layer=Layer.APPLICATION, application=Application.MAIL)
m.add_many({
    LayerBinding('spacebar'): {
        mail: [Binding('f', {'left_alt', 'command'}),
               SetMode(Mode.SEARCH)]
    },
    LayerBinding('f'): {
        mail: [Binding('f', {'command', 'left_shift'}),
               SetMode(Mode.INSERT)]
    },
    LayerBinding('r'): {
        mail: [Binding('r', {'command'}),
               SetMode(Mode.INSERT)]
    },
    LayerBinding('r', {'shift'}): {
        mail: [Binding('r', {'command', 'left_shift'}),
               SetMode(Mode.INSERT)]
    },
    LayerBinding('a'): {
        mail: Binding('a', {'command', 'right_control'})
    },
    LayerBinding('l'): {
        mail: Binding('l', {'command', 'left_shift'})
    },
    LayerBinding('return_or_enter'): {
        mail: Binding('d', {'command', 'left_shift'})
    },
})

for key, direction in (
    ('h', 'west'),
    ('j', 'south'),
    ('k', 'north'),
    ('l', 'east'),
):
    m.add(
        LayerBinding(key),
        {Context(layer=Layer.WINDOW): Command.focus_window(direction)},
    )
    m.add(
        LayerBinding(key).add_shift(),
        {Context(layer=Layer.WINDOW): Command.move_window(direction)},
    )

for key, direction in (
    ('u', 'prev'),
    ('p', 'next'),
):
    m.add(
        LayerBinding(key),
        {Context(layer=Layer.WINDOW): Command.focus_space(direction)},
    )
    m.add(
        LayerBinding(key).add_shift(),
        {Context(layer=Layer.WINDOW): Command.move_to_space(direction)},
    )

# arrow keys, words
for binding, direction in (
    (('h', ), Binding.left()),
    (('j', ), Binding.down()),
    (('k', ), Binding.up()),
    (('l', ), Binding.right()),
    (('w', ), Binding.word_forward()),
    (('e', ), Binding.word_forward()),
    (('b', ), Binding.word_back()),
    (('0', ), Binding.begin_of_line()),
    (('4', {'shift'}), Binding.end_of_line()),
):
    m.add(
        Binding(*binding), {
            Context(Mode.NORMAL):
            direction,
            Context(Mode.NORMAL, operator=Operator.CHANGE): [
                direction.add_shift(),
                Binding.cut(),
                SetOperator(Operator.NORMAL),
                SetMode(Mode.INSERT),
            ],
            Context(Mode.NORMAL, operator=Operator.DELETE): [
                direction.add_shift(),
                Binding.cut(),
                SetOperator(Operator.NORMAL),
            ],
            Context(Mode.NORMAL, operator=Operator.YANK): [
                direction.add_shift(),
                Binding.copy(),
                SetOperator(Operator.NORMAL),
            ],
            Context(Mode.VISUAL):
            direction.add_shift(),
        })

# insert arrows
for key, direction in (
    ('h', Binding.left()),
    ('j', Binding.down()),
    ('k', Binding.up()),
    ('l', Binding.right()),
):
    m.add(
        Binding(key, {'right_control'}), {
            Context(Mode.INSERT): direction,
            Context(Mode.NATIVE): direction,
            Context(Mode.SEARCH): direction,
        })

for binding, direction in (
    (('u', {'control'}), Binding.page_up()),
    (('d', {'control'}), Binding.page_down()),
    (('g', ), Binding.page_top()),
    (('g', {'left_shift'}), Binding.page_bottom()),
):
    m.add(
        Binding(*binding), {
            Context(Mode.NORMAL): direction,
            Context(Mode.VISUAL): direction.add_shift(),
        })

# yank, delete
for key, action, op in (
    ('d', Binding.cut(), Operator.DELETE),
    ('y', Binding.copy(), Operator.YANK),
):
    m.add(
        Binding(key), {
            Context(Mode.NORMAL):
            SetOperator(op),
            Context(Mode.NORMAL, operator=op): [
                Binding.begin_of_line(),
                Binding.end_of_line().add_shift(),
                action,
                SetOperator(Operator.NORMAL),
            ],
            Context(Mode.VISUAL): [
                action,
                SetMode(Mode.NORMAL),
            ],
        })

ultra = {
    'title': 'Ultra',
    'author': 'Constantine Theocharis',
    'rules': [{
        'description': 'Ultra',
        'manipulators': m.manipulators,
    }]
}

with open(
        realpath(
            '~/.config/karabiner/assets/complex_modifications/ultra.json'),
        'w') as f:
    json.dump(ultra, f)

p.local['common_json'] = json.dumps(common)
p.local['spoon_path'] = realpath('~/.hammerspoon/Spoons/Ultra.spoon')
p.install({'ultra_hs.lua': '~/.hammerspoon/Spoons/Ultra.spoon/init.lua'})
p.execute(
    'cp -r menubar-shapes ~/.hammerspoon/Spoons/Ultra.spoon/menubar-shapes')
logger.info('Installed ultra karabiner config')
