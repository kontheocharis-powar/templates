scripts = ('terminal', 'swapspaces')
path_scripts = ('terminal', )
subdir = 'macos'

p.install({ x: f'~/.scripts/{subdir}/{x}' for x in scripts})
p.link({f'~/.scripts/{subdir}/{s}' : f'~/.scripts/path/{s}' for s in path_scripts})
