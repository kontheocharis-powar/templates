"""
Send sequences to all open terminals.
"""
import glob
import os
import sys
import argparse
import yaml


def set_special(index, color, alpha=100):
    """Convert a hex color to a special sequence."""
    if index in [11, 708] and alpha != 100:
        return "\033]%s;[%s]#%s\033\\" % (index, alpha, color)

    return "\033]%s;#%s\033\\" % (index, color)


def set_color(index, color):
    """Convert a hex color to a text color sequence."""
    return "\033]4;%s;#%s\033\\" % (index, color)


def create_sequences(colors, vte_fix=False):
    """Create the escape sequences."""

    # Colors 0-15.
    sequences = [
        set_color(index, colors["color%s" % index]) for index in range(16)
    ]

    # Special colors.
    # Source: https://goo.gl/KcoQgP
    # 10 = foreground, 11 = background, 12 = cursor foregound
    # 13 = mouse foreground, 708 = background border color.
    sequences.extend([
        set_special(10, colors["foreground"]),
        set_special(11, colors["background"]),
        set_special(12, colors["cursor"]),
        set_special(13, colors["foreground"]),
        set_special(17, colors["foreground"]),
        set_special(19, colors["background"]),
        set_color(232, colors["background"]),
        set_color(256, colors["foreground"])
    ])

    if not vte_fix:
        sequences.extend(set_special(708, colors["background"]))

    return ''.join(sequences)


def send(colors, vte_fix=False):
    """Send colors to all open terminals."""
    tty_pattern = "/dev/ttys00[0-9]*"
    sequences = create_sequences(colors, vte_fix)

    # Writing to "/dev/pts/[0-9] lets you send data to open terminals.
    for term in glob.glob(tty_pattern):
        with open(term, "w") as f:
            f.write(sequences)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Send escape codes ')
    parser.add_argument(
        'file',
        nargs='?',
        help=
        'File to read colors from (in yml format). Otherwise, reads from stdin.',
    )

    args = parser.parse_args()

    if args.file:
        with open(args.file, 'r') as f:
            content = yaml.safe_load(f.read())
    else:
        content = yaml.safe_load(sys.stdin.read())

    send(content)
