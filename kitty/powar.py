# system_packages:
#   - kitty

import logging
import reload_colors

logger: logging.Logger = logging.getLogger(__name__)

p.install({'kitty.conf': '~/.config/kitty/kitty.conf'})
p.execute('kitty @ --to=unix:/tmp/kitty-socket set-colors -a -c ~/.config/kitty/kitty.conf')

# logger.info(p.opts['colors'])
# reload_colors.send(p.opts['colors'])
# logger.info('Reloaded terminal colors')
