# system_packages:
#   - zsh
#   - oh-my-zsh
#   - fast-syntax-highlighting

from os import path

p.install({
    'zshenv-home': '~/.zshenv',
    'zshenv': '~/.config/zsh/.zshenv',
    'zshrc': '~/.config/zsh/.zshrc',
    'zprofile': '~/.config/zsh/.zprofile',
    'aliases': '~/.config/local/aliases',
    'profile': '~/.config/local/profile',
    'vimode.zsh': '~/.config/local/vimode.zsh',
})
