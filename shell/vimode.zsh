# vi mode
bindkey -v

# Remove delay when entering normal mode (vi)
KEYTIMEOUT=1

# Change cursor shape for different vi modes.
function zle-keymap-select {
    if [[ $KEYMAP == vicmd ]] || [[ $1 = 'block' ]]; then
        echo -ne "\e[1 q"
    elif [[ $KEYMAP == main ]] || [[ $KEYMAP == viins ]] || [[ $KEYMAP = '' ]] || [[ $1 = 'beam' ]]; then
        echo -ne "\e[5 q"
    fi
}
zle -N zle-keymap-select
# Start with beam shape cursor on zsh startup and after every command.
zle-line-init() { zle-keymap-select 'beam'}

# visual mode
bindkey -M vicmd "v" visual-mode
bindkey -M vicmd "^V" edit-command-line

bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey '^[[Z' reverse-menu-complete

# Blank lines on enter

FIRST_EXEC=1

function echo_blank() {
    echo
}

function echo_blank_precmd() {
    if [[ $FIRST_EXEC = 1 ]]; then
        FIRST_EXEC=0
    else
        echo
    fi
}

preexec_functions+=echo_blank
precmd_functions+=echo_blank_precmd
